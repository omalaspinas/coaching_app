#!/bin/sh

echo "======================================="
echo "Currently Running all engine CI locally"
echo "======================================="

cd engine
mv -b *.db ./tmp/

# compilation
cargo build --bin main
cargo build --bin generate_pdf --features="pdf"
cargo build --bin get_images
cargo build --bin add_exercise_descriptor --features="cli"
cargo build --bin add_athlete --features="cli"

# test
cargo test
cargo test --features=cli
cargo test --features=pdf

#doc
cargo doc --all-features

#clippy
cargo clippy --all-targets --all-features -- -D warnings

#run examples
cargo run --bin main
cargo run --bin generate_pdf --features="pdf" -- tmp/test.pdf
cargo run --bin get_images
cargo run --bin add_exercise_descriptor --features="cli" -- --name "Run" --output "test.db"
cargo run --bin add_athlete --features="cli" -- --name "Jean-Paul" --surname "Dax" --birthdate "9-10-2000"

