// SPDX-FileCopyrightText: 2020 Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: CC0-1.0

//! This example generates a demo PDF document and writes it to the path that was passed as the
//! first command-line argument.  You may have to adapt the `FONT_DIRS`, `DEFAULT_FONT_NAME` and
//! `MONO_FONT_NAME` constants for your system so that these files exist:
//! - `{FONT_DIR}/{name}-Regular.ttf`
//! - `{FONT_DIR}/{name}-Bold.ttf`
//! - `{FONT_DIR}/{name}-Italic.ttf`
//! - `{FONT_DIR}/{name}-BoldItalic.ttf`
//! for `name` in {`DEFAULT_FONT_NAME`, `MONO_FONT_NAME`}.
//!
//! The generated document using the latest `genpdf-rs` release is available
//! [here](https://genpdf-rs.ireas.org/examples/demo.pdf).

use anyhow::{Context, Result};
use engine::exercise_descriptor::ExerciseDescriptor;
use engine::exercise_qualification::{Intensity, Quantity};
use engine::pdf::PdfGenerator;
use engine::{Exercise, Repetition, TrainingSession};
use std::env;

fn create_training_session() -> Result<TrainingSession> {
    let run = ExerciseDescriptor::new("Run").set_description(&String::from(
        "One foot in front of another and again until the end of times.",
    ));
    let warming = Exercise::new(
        run.clone(),
        Quantity::from_hours_minutes_seconds(0, 15, 0)
            .context("Could not create duration from 15 minutes and 0 seconds")?,
        Intensity::from_percentage(70)
            .context("Impossible to create an intensity from a 70 percentage")?,
    );

    let rest = ExerciseDescriptor::new("Rest");
    let sprint_ex = Exercise::new(
        run,
        Quantity::from_meters(100.0).context("Could not create Quantity from 100.0 meters")?,
        Intensity::from_percentage(100)
            .context("Impossible to create an intensity from a 100 percentage")?,
    );
    let rest_ex = Exercise::new(
        rest,
        Quantity::from_meters(100.0).context("Could not create Quantity from 100.0 meters")?,
        Intensity::none(),
    );
    let squat = ExerciseDescriptor::new("Squat").set_description(&String::from("Standard running."))
            .add_photo("https://43nnuk1fz4a72826eo14gwfb-wpengine.netdna-ssl.com/wp-content/uploads/2016/10/ArmStanding.jpg")
            .add_photo("https://3i133rqau023qjc1k3txdvr1-wpengine.netdna-ssl.com/wp-content/uploads/2014/08/Basic-Squat_Exercise.jpg");
    let squat_ex = Exercise::new(
        squat,
        Quantity::from_repetitions(10).context("Could not create Quantity from 10 repetitions")?,
        Intensity::none(),
    );
    // let photos: Vec<_> = squat.download_photos().into_iter().flatten().collect();
    let rep_warming = Repetition::new().add_exercise(warming);

    let rep_sprints = Repetition::new()
        .add_exercise(sprint_ex)
        .add_exercise(rest_ex)
        .set_amount(10)
        .context("Failed to create sprint repetitions")?;
    let rep_squats = Repetition::new().add_exercise(squat_ex);
    let training = TrainingSession::new()
        .add_repetition(rep_warming.clone())
        .add_repetition(rep_sprints.clone())
        .add_repetition(rep_warming.clone())
        .add_repetition(rep_warming.clone())
        .add_repetition(rep_squats)
        .add_repetition(rep_sprints)
        .add_repetition(rep_warming);
    Ok(training)
}

fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();
    if args.len() != 1 {
        panic!("Missing argument: output file");
    }
    let output_file = &args[0];

    let ts = create_training_session()?;
    PdfGenerator::from_ts(output_file, &ts)?;
    Ok(())
}
