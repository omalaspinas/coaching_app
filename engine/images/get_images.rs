use engine::exercise_descriptor::ExerciseDescriptor;

fn main() {
    // env_logger::init();
    let squat =
        ExerciseDescriptor::new("Squat").set_description(&String::from("Standard running."))
            .add_photo("https://43nnuk1fz4a72826eo14gwfb-wpengine.netdna-ssl.com/wp-content/uploads/2016/10/ArmStanding.jpg")
            .add_photo("https://3i133rqau023qjc1k3txdvr1-wpengine.netdna-ssl.com/wp-content/uploads/2014/08/Basic-Squat_Exercise.jpg");
    squat
        .download_photos()
        .into_iter()
        .flatten()
        .enumerate()
        .for_each(|(e, (pho, ext))| {
            let e_str = ext.extensions_str().get(0).unwrap();
            pho.save(format!("photo{}.{}", e, e_str)).unwrap();
        });
}
