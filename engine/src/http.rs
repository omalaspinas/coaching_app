//! Here we handle everything related to http request for the moment.

use image::io::Reader as ImageReader;
use image::{DynamicImage, ImageFormat};
use std::io::Cursor;

/// This function gets a string formatted url and returns an [`DynamicImage`] from the `image`
/// crate and its [`ImageFormat`].
pub fn img_from_str(url: &str) -> Option<(DynamicImage, ImageFormat)> {
    if let Ok(r) = attohttpc::RequestBuilder::try_new(attohttpc::Method::GET, url) {
        if let Ok(r) = r.send() {
            if r.is_success() {
                if let Ok(c) = r.bytes() {
                    if let Ok(reader) = ImageReader::new(Cursor::new(c)).with_guessed_format() {
                        match (reader.format(), reader.decode().ok()) {
                            (Some(f), Some(i)) => Some((i, f)),
                            (_, _) => None,
                        }
                    } else {
                        None
                    }
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    } else {
        None
    }
}
