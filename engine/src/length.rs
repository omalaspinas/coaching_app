//! This module contains distance metrics in meters.
//!
//! This will be useful for all the exercises that are based on distance (e.g. running, swimming,
//! cycling, ...).

use ::serde::{Deserialize, Serialize};
use thiserror::Error;

/// Only meters. They are used as the metric for a distance in this crate.
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Meters(f64);

/// `Meters` implements only a very limited amount of functions.
///
/// Meters can be constructed from meters or kilometers.
///
/// # Examples:
///
/// ```
/// use engine::length::Meters;
///
/// let five_hundred = Meters::from_meters(500.0);
/// assert_eq!(five_hundred.unwrap(), Meters::from_kilometers(0.5).unwrap());
/// ```
impl Meters {
    /// Constructs meters from a floating point number
    /// in kilometers.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::length::Meters;
    ///
    /// let length = Meters::from_kilometers(80.0).unwrap(); // 80000m length
    /// assert_eq!(f64::from(&length), 80000.0);
    /// ```
    pub fn from_kilometers(km: f64) -> Result<Self, LengthError> {
        Meters::from_meters(km * 1000.0)
    }

    /// Contructs meters from meters (noting to do).
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::length::Meters;
    ///
    /// let length = Meters::from_meters(80.0).unwrap(); // 80m length
    /// assert_eq!(f64::from(&length), 80.0);
    /// ```
    pub fn from_meters(meters: f64) -> Result<Self, LengthError> {
        if meters < 0.0 {
            Err(LengthError::PositivityError(meters))
        } else {
            Ok(Meters(meters))
        }
    }

    /// Contructs meters from meters (noting to do).
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::length::Meters;
    ///
    /// let length = Meters::from_centimeters(80).unwrap(); // 0.80m length
    /// assert_eq!(f64::from(&length), 0.80);
    /// ```
    pub fn from_centimeters(centimeters: i64) -> Result<Self, LengthError> {
        Meters::from_meters(centimeters as f64 / 100.0)
    }
}

/// Implementation of the From trait
impl From<&Meters> for f64 {
    fn from(item: &Meters) -> Self {
        item.0
    }
}

/// The formatter for the [`Meters`] struct.
impl std::fmt::Display for Meters {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} meters", self.0)
    }
}

/// The Error manager for lengths
#[derive(Error, Debug)]
pub enum LengthError {
    /// The positivity of the length
    #[error("The length must always be positive, but got {0}")]
    PositivityError(f64),
}
