//!
//! This crate is the basic engine to create training series that can be running, swimming,
//! weight-lifting, etc.
//!
//! The crate is split in different parts.
//!
//! 1. A single [`Exercise`] which is made of several components
//!
//!     * [`ExerciseDescriptor`]: which describes an exercise.
//!     * [`Quantity`]: which describes how many (in terms of time, distance, repetitions) an [`ExerciseDescriptor`]
//!        can be performed.
//!     * [`Intensity`]: what the intensity of the [`ExerciseDescriptor`] must be (in terms of percentage of maximum heart-rate, time,
//!        or additional charge).
//! 2. A [`Repetition`] which is a certain `amount` of an ordered sequence of [`Exercise`]s.
//! 3. A [`TrainingSession`] which is an ordered sequence of [`Repetition`]s.
//!
//!
//!

#![deny(missing_docs)]
#![deny(broken_intra_doc_links)]

pub mod athlete;
pub mod duration;
pub mod exercise_descriptor;
pub mod exercise_qualification;
pub mod http;
pub mod length;
#[cfg(feature = "pdf")]
pub mod pdf;
pub mod weight;

use exercise_descriptor::ExerciseDescriptor;
use exercise_qualification::{Intensity, Quantity};
use std::collections::HashSet;
use thiserror::Error;

/// The [`Exercise`] is the basic building block of each training.
///
/// It is a composition fo three quantities:
/// * An [`ExerciseDescriptor`] which describes the exercise.
/// * A [`Quantity`] which is the "amount" of exercises to be performed.
/// * An [`Intensity`].
#[derive(Debug, Clone)]
pub struct Exercise {
    ex: ExerciseDescriptor,
    quantity: Quantity,
    intensity: Intensity,
}

impl Exercise {
    /// All fieldr must be initialized with known quantities.
    /// There are no value by default here and all arguments are moved.
    ///
    /// # Arguments
    ///
    /// - ex: the description of the exercise
    /// - quantity: how much of the exercise must be performed
    /// - intensity: at which intensity the exercise must be performed
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::Exercise;
    /// # use engine::exercise_qualification::{Quantity, Intensity};
    /// # use engine::duration::Duration;
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    ///
    /// let warming = Exercise::new(
    ///     ExerciseDescriptor::new("Run").set_description(&String::from(
    ///         "One foot in front of another and again until the end of times.",
    ///     )),
    ///     Quantity::from_hours_minutes_seconds(0, 15, 0).unwrap(),
    ///     Intensity::from_percentage(70)
    ///         .unwrap(),
    /// );
    /// ```
    pub fn new(ex: ExerciseDescriptor, quantity: Quantity, intensity: Intensity) -> Self {
        Exercise {
            ex,
            quantity,
            intensity,
        }
    }
}

/// The formatter for the [`Exercise`] struct.
impl std::fmt::Display for Exercise {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let q_str = format!("{}", self.quantity);
        let i_str = format!("{}", self.intensity);
        if !q_str.is_empty() && !i_str.is_empty() {
            write!(f, "{} for {} at {}", self.ex, q_str, i_str)
        } else if !q_str.is_empty() {
            write!(f, "{} {}", self.ex, q_str)
        } else if !i_str.is_empty() {
            write!(f, "{} at {}", self.ex, i_str)
        } else {
            write!(f, "{}", self.ex)
        }
    }
}

/// The [`Repetition`] describes an ordered set of [`Exercise`]s with a possible number of
/// repetition. If the amount is `None` then only one repetition is performed.
///
/// # Example
///
/// In other terms a repeptition can be a succession of exercises that must be performed a given
/// amount of times like:
///
/// 1: Run for 30 seconds at 100%
/// 2: Walk for 30 seconds 1 time
/// 3: Run for 1 minute at 100%
/// 4: Walk for 1 minute 1 time
/// Repeat 1-4 four times
///
/// ```no_run
/// # use engine::Repetition;
/// # use engine::Exercise;
/// # use engine::exercise_qualification::{Quantity, Intensity};
/// # use engine::duration::Duration;
/// # use engine::exercise_descriptor::ExerciseDescriptor;
///
/// let r_desc = ExerciseDescriptor::new("Run");
/// let w_desc = ExerciseDescriptor::new("Walk");
///
/// let run_30 = Exercise::new(
///     r_desc.clone(),
///     Quantity::from_hours_minutes_seconds(0,0,30).unwrap(),
///     Intensity::from_percentage(100).unwrap(),
/// );
/// let walk_30 = Exercise::new(
///     w_desc.clone(),
///     Quantity::from_hours_minutes_seconds(0,0,30).unwrap(),
///     Intensity::from_percentage(50).unwrap(),
/// );
/// let run_1 = Exercise::new(
///     r_desc.clone(),
///     Quantity::from_hours_minutes_seconds(0,1,0).unwrap(),
///     Intensity::from_percentage(100).unwrap(),
/// );
/// let walk_1 = Exercise::new(
///     w_desc.clone(),
///     Quantity::from_hours_minutes_seconds(0,1,0).unwrap(),
///     Intensity::from_percentage(50).unwrap(),
/// );
///
/// let rep =
///     Repetition::new()
///         .add_exercise(run_30).add_exercise(walk_30).add_exercise(run_1).add_exercise(walk_1)
///         .set_amount(4);
/// ```
#[derive(Debug, Clone)]
pub struct Repetition {
    /// List of [`Exercise`]s in the order they are supposed to be performed.
    set: Vec<Exercise>,
    /// The number of repetitions the sequence of [`Exercise`]s must be performed.
    /// If the [`Option`] is `None` then only one repetition will be performed.
    amount: Option<u32>,
}

impl Repetition {
    /// Creates a new [`Repetition`] with no [`Exercise`] and [`enum@Option::None`] amount.
    /// By default no amount is specivied and there is no exercise.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::Repetition;
    ///
    /// let rep = Repetition::new();
    /// ```
    pub fn new() -> Self {
        Repetition {
            set: Vec::new(),
            amount: None,
        }
    }

    /// Sets the amount of repetitions to be performed.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::Repetition;
    /// # use engine::Exercise;
    /// # use engine::exercise_qualification::{Quantity, Intensity};
    /// # use engine::duration::Duration;
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    ///
    /// let warming = Exercise::new(
    ///     ExerciseDescriptor::new("Run"),
    ///     Quantity::from_hours_minutes_seconds(0, 15, 0).unwrap(),
    ///     Intensity::from_percentage(70)
    ///         .unwrap(),
    /// );
    ///
    /// let rep = Repetition::new().add_exercise(warming).set_amount(10);
    /// ```
    pub fn set_amount(mut self, amount: u32) -> Result<Self, RepetitionError> {
        if !self.set.is_empty() {
            self.amount = Some(amount);
            Ok(self)
        } else {
            Err(RepetitionError::AmountError)
        }
    }

    /// Adds an exercise to the repetition.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::Repetition;
    /// # use engine::Exercise;
    /// # use engine::exercise_qualification::{Quantity, Intensity};
    /// # use engine::duration::Duration;
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    ///
    /// let warming = Exercise::new(
    ///     ExerciseDescriptor::new("Run"),
    ///     Quantity::from_hours_minutes_seconds(0, 15, 0).unwrap(),
    ///     Intensity::from_percentage(70)
    ///         .unwrap(),
    /// );
    ///
    /// let rep = Repetition::new().add_exercise(warming);
    /// ```
    pub fn add_exercise(mut self, ex: Exercise) -> Self {
        self.set.push(ex);
        self
    }

    /// Returns an iterator over the set of Exercises in the repetition
    pub fn iter(&self) -> std::slice::Iter<'_, Exercise> {
        self.set.iter()
    }

    /// Returns the amount of repetitions. By default this returns 1 when no amount of repetitions
    /// is specified.
    pub fn num_repetitions(&self) -> u32 {
        self.amount.unwrap_or(1)
    }

    /// Returns a vector with each item a line of the repetition
    /// If there is only one line it means there is only 1 exercise
    /// in the repetition. If not there are more than one and they must
    /// be treated accordingly.
    pub fn lines(&self) -> Vec<Format> {
        let mut l: Vec<_> = self
            .iter()
            .map(|s| Format::Default(format!("{}", s)))
            .collect();

        l.push(Format::Italic(format!(
            "Repeat {} times",
            self.num_repetitions()
        )));
        l
    }
}

/// Used for the formatting of some string
pub enum Format {
    /// Default police
    Default(String),
    /// Bold police
    Bold(String),
    /// Italic police
    Italic(String),
    /// An Image
    Image(String),
}

impl Default for Repetition {
    fn default() -> Self {
        Self::new()
    }
}

impl std::fmt::Display for Repetition {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for s in self.lines() {
            match s {
                Format::Default(a) | Format::Bold(a) | Format::Italic(a) | Format::Image(a) => {
                    writeln!(f, "{}", a)?
                }
            }
        }
        Ok(())
    }
}

/// The Error manager for Athlete
#[derive(Error, Debug)]
pub enum RepetitionError {
    /// Certain keys must be present in the Athlete's Db
    #[error("There are no exercises in the Repetition. One can't add an amount of anything.")]
    AmountError,
}
/// The [`TrainingSession`] is an ordered sequence of [`Repetition`]s.
/// In other terms it is a vector of repetitions that can have an arbitrary
/// number of it.
///
/// For the moment there are no examples for the TrainingSession because the API is very
/// experimental and there is no . But in time it will come.
#[derive(Debug)]
pub struct TrainingSession {
    /// The ordered [`Repetition`]s.
    reps: Vec<Repetition>,
}

impl TrainingSession {
    /// Creates a new [`TrainingSession`] which is empty by default.
    pub fn new() -> Self {
        TrainingSession { reps: Vec::new() }
    }

    /// Adds a [`Repetition`] to the [`TrainingSession`].
    pub fn add_repetition(mut self, rep: Repetition) -> Self {
        self.reps.push(rep);
        self
    }

    /// Returns an iterator over the [`Repetition`]s tobe performed in the traning session
    pub fn iter(&self) -> std::slice::Iter<'_, Repetition> {
        self.reps.iter()
    }

    /// Returns a HashSet with all the different exercise descriptors.
    /// The HashSet is just the list of the unique exercises to be performed.
    ///
    /// This is useful for the PDF export for example when one wants to show
    /// pictures of the different exercises.
    pub fn get_exercise_descriptors(&self) -> HashSet<ExerciseDescriptor> {
        let mut hs = HashSet::new();
        for rep in self.iter() {
            for ex in rep.iter() {
                hs.insert(ex.ex.clone());
            }
        }
        hs
    }

    /// Returns a format with the header of the exercise
    pub fn lines(&self) -> Vec<Format> {
        (1..=self.reps.len())
            .into_iter()
            .map(|s| Format::Bold(format!("Exercise {}", s)))
            .collect()
    }
}

impl std::fmt::Display for TrainingSession {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for lts in self.lines() {
            match lts {
                Format::Default(a) | Format::Bold(a) | Format::Italic(a) | Format::Image(a) => {
                    writeln!(f, "{}", a)?
                }
            }
            for r in self.reps.iter() {
                writeln!(f, "{}", r)?;
            }
        }
        Ok(())
    }
}

impl Default for TrainingSession {
    fn default() -> Self {
        Self::new()
    }
}
