//! In this module we define the [`ExerciseDescriptor`] structure that contains a description of
//! the possible execises.
//!
//! We also will handle the the output to a very simple JSon-type database. It will be handled
//! through the [`PickleDb`] crate.

use crate::Format;
use image::{DynamicImage, ImageFormat};
use pickledb::{PickleDb, PickleDbDumpPolicy};
use serde::{Deserialize, Serialize};
#[cfg(feature = "cli")]
use structopt::StructOpt;
use thiserror::Error;

/// Builder for CLI interface
#[cfg(feature = "cli")]
#[derive(Debug, StructOpt)]
#[structopt(name = "basic")]
pub struct ExerciseDescriptorBuilder {
    // A flag, true is used in the command line. Replaces the exercise
    // in the Db if it already exists, or creates a new entry if non existent.
    /// Force replace exercise into database
    #[structopt(long)]
    replace: bool,
    /// The name of the exercise.
    #[structopt(short, long)]
    name: String,
    /// The name of the output file of the exercise Db.
    #[structopt(short, long)]
    output: String,
    /// An accurate description of the exercise.
    #[structopt(long)]
    description: Option<String>,
    /// Possible tags of the exercise to know in what category the exercice is part of.
    #[structopt(short, long)]
    tags: Vec<String>,
    /// A possible list of photos of the exercise.
    #[structopt(short, long)]
    photos: Vec<String>,
    /// A possible list of videos of the exercise.
    #[structopt(short, long)]
    videos: Vec<String>,
}

#[cfg(feature = "cli")]
impl ExerciseDescriptorBuilder {
    /// Creates an ExerciseDescriptor from its associated builder which is constructed with the
    /// StructOpt::from_args() function. Returns an error if there was a problem.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptorBuilder;
    /// # use structopt::StructOpt;
    ///
    /// let ex = ExerciseDescriptorBuilder::from_args().finalize().unwrap();
    /// ```
    pub fn finalize(self) -> Result<(), ExerciseDescriptorError> {
        let ex = ExerciseDescriptor::new(&self.name);
        let mut ex = match self.description {
            Some(desc) => ex.set_description(&desc),
            None => ex,
        };
        for tag in self.tags.iter() {
            ex = ex.add_tag(tag);
        }
        for photo in self.photos.iter() {
            ex = ex.add_photo(photo);
        }
        for video in self.videos.iter() {
            ex = ex.add_video(video);
        }

        let mut db = ExerciseDescriptorDb::load_or_new(&self.output);
        if self.replace {
            db.add_or_replace_exercise(&ex)?;
        } else {
            db.try_add_exercise(&ex)?;
        }

        Ok(())
    }
}

/// An [`ExerciseDescriptor`] is a structure that only contains the description of an exercise.
/// It is made of two mandatory fields:
/// * a `name`;
/// * a `description`.
///
/// It can also contain two fields that can help the sportsman perform the exercises:
/// * `tags`;
/// * `photos`;
/// * `videos`.
///
/// The [`ExerciseDescriptor`] can be (de)serialized.
#[derive(Debug, Hash, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct ExerciseDescriptor {
    /// The name of the exercise.
    pub name: String,
    /// An accurate description of the exercise.
    pub description: Option<String>,
    /// Possible tags of the exercise to know in what category the exercice is part of.
    tags: Vec<String>,
    /// A possible list of photos of the exercise.
    photos: Vec<String>,
    /// A possible list of videos of the exercise.
    videos: Vec<String>,
}

impl ExerciseDescriptor {
    /// Create a new ExerciseDescriptor which must contain two mandatory fields:
    /// a `name` and a `description`.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// let e_d = ExerciseDescriptor::new("What an exercise!");
    /// ```
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::{ExerciseDescriptor, ExerciseDescriptorError};
    /// # fn main() -> Result<(), ExerciseDescriptorError> {
    /// // The description is from wikipedia
    /// let e_d = ExerciseDescriptor::new("Squat").add_tag("Strength").add_tag("Running")
    ///     .try_set_description("A squat is a strength exercise in which the trainee lowers
    ///         their hips from a standing position and then stands back up. During the
    ///         descent of a squat, the hip and knee joints flex while the ankle joint
    ///         dorsiflexes; conversely the hip and knee joints extend and the ankle
    ///         joint plantarflexes when standing up.")?
    ///     .add_photo("https://upload.wikimedia.org/wikipedia/commons/6/6a/Kniebeuge.jpg");
    /// Ok(())
    /// # }
    /// ```
    pub fn new(name: &str) -> Self {
        ExerciseDescriptor {
            name: String::from(name),
            description: None,
            tags: Vec::new(),
            photos: Vec::new(),
            videos: Vec::new(),
        }
    }

    /// Optionally one can add tags to the exercise.
    /// This feature is not yet used. Probably this will
    /// be made into an enum.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// let e_d = ExerciseDescriptor::new("Squat")
    ///     .add_tag("Running")
    ///     .add_tag("Strength");
    /// ```
    pub fn add_tag(mut self, tag: &str) -> Self {
        self.tags.push(String::from(tag));
        self
    }

    /// Optionally one can owerwrite the description.
    ///
    /// # Argument
    ///
    /// - desc: A description of the exercise.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// // The description is from wikipedia
    /// let e_d = ExerciseDescriptor::new("Squat")
    ///     .set_description("A squat is a strength exercise in which the trainee lowers
    ///         their hips from a standing position and then stands back up. During the
    ///         descent of a squat, the hip and knee joints flex while the ankle joint
    ///         dorsiflexes; conversely the hip and knee joints extend and the ankle
    ///         joint plantarflexes when standing up.");
    /// // Do some stuff
    /// let e_d = e_d.set_description("Set a new description.");
    /// ```
    pub fn set_description(mut self, desc: &str) -> Self {
        self.description = Some(String::from(desc));
        self
    }

    /// Optionally one can try to set a description.
    /// If a description is already present then an error is produced.
    ///
    /// # Argument
    ///
    /// - desc: A description of the exercise.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// // The description is from wikipedia
    /// let e_d = ExerciseDescriptor::new("Squat")
    ///     .try_set_description("A squat is a strength exercise in which the trainee lowers
    ///         their hips from a standing position and then stands back up. During the
    ///         descent of a squat, the hip and knee joints flex while the ankle joint
    ///         dorsiflexes; conversely the hip and knee joints extend and the ankle
    ///         joint plantarflexes when standing up.").unwrap(); // this works
    /// // Do some stuff
    /// e_d.try_set_description("Try to set another description.").unwrap(); // this will fail
    /// ```
    pub fn try_set_description(mut self, desc: &str) -> Result<Self, ExerciseDescriptorError> {
        match self.description {
            Some(_) => Err(ExerciseDescriptorError::AlreadyPresentDescrptionError),
            None => {
                self.description = Some(String::from(desc));
                Ok(self)
            }
        }
    }

    /// Optionally one can add a photo.
    ///
    /// # Argument
    ///
    /// - photo : the url of a photo.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// let e_d = ExerciseDescriptor::new("Squat")
    ///     .add_photo("https://upload.wikimedia.org/wikipedia/commons/6/6a/Kniebeuge.jpg");
    /// ```
    pub fn add_photo(mut self, photo: &str) -> Self {
        self.photos.push(String::from(photo));
        self
    }

    /// Optionally one can add a video.
    ///
    /// # Argument
    ///
    /// - video: the url of a video
    ///
    /// This feature is not yet used.
    #[allow(dead_code)]
    fn add_video(mut self, video: &str) -> Self {
        self.videos.push(String::from(video));
        self
    }

    /// Get the id of the [`ExerciseDescriptor`]
    ///
    /// For the moment the id is the name of the [`ExerciseDescriptor`].
    ///
    /// # Example
    ///
    /// ```
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// let e_d = ExerciseDescriptor::new("Squat");
    /// assert_eq!(e_d.get_id(), "Squat");
    /// ```
    pub fn get_id(&self) -> String {
        self.name.clone()
    }

    /// Download the [`ExerciseDescriptor::photos`] from the Vec<String> that contains their URL
    ///
    /// This function returns a Vec with all the donwloaded photos and their format into a tuple.
    /// If the download fails for any reason the content is filled with None.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// let e_d = ExerciseDescriptor::new("Squat")
    ///     .add_photo("https://upload.wikimedia.org/wikipedia/commons/6/6a/Kniebeuge.jpg");
    /// let photos: Vec<_> = e_d.download_photos();
    /// ```
    pub fn download_photos(&self) -> Vec<Option<(DynamicImage, ImageFormat)>> {
        self.photos
            .iter()
            .map(|p_str| crate::http::img_from_str(p_str))
            .collect()
    }

    /// Returns the columns that are to be put in a table with the description
    pub fn columns(&self, num_cols: u32) -> Vec<Format> {
        let mut c = vec![Format::Default(self.name.clone())];
        c.push(Format::Default(match self.description {
            None => String::from(""),
            Some(ref s) => s.clone(),
        }));
        for (i, p) in self.photos.iter().enumerate() {
            if i as u32 == num_cols - 2 {
                break;
            }
            c.push(Format::Image(p.clone()));
        }
        (c.len() as u32..num_cols).for_each(|_| c.push(Format::Default(String::from(""))));

        c
    }
}

/// The formatter for the [`ExerciseDescriptor`] enum.
impl std::fmt::Display for ExerciseDescriptor {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}

/// The Error handling for the ExerciseDescriptor
#[derive(Debug, Error)]
pub enum ExerciseDescriptorError {
    /// The description field is already present
    #[error("The description is already present.")]
    AlreadyPresentDescrptionError,

    /// Propagating the error form PickleDb opening
    #[error(transparent)]
    PickleDbError(#[from] pickledb::error::Error),

    /// Propagating the error form ExerciseDescriptorDbError opening
    #[error(transparent)]
    ExerciseDescriptorDbError(#[from] ExerciseDescriptorDbError),
}

/// An [`ExerciseDescriptorDb`] is a structure that only contains the databse of [`ExerciseDescriptor`]s.
///
/// It is designed to be as simple as possible, so it's just a wrapper around [`PickleDb`].
/// The identifier (key) of each exercise is its name.
/// Adding a key for a second time overwrites the previous entry in [`PickleDb`].
///
/// # Examples
///
/// ```no_run
/// # use engine::exercise_descriptor::{ExerciseDescriptor, ExerciseDescriptorDb};
/// let mut db = ExerciseDescriptorDb::new("ex.db");
/// let rest = ExerciseDescriptor::new("Rest")
///     .set_description("Do nothing");
/// let run = ExerciseDescriptor::new("Run");
///
/// db.try_add_exercise(&run).unwrap(); // Ok
/// db.add_or_replace_exercise(&rest).unwrap(); // Ok
/// db.try_add_exercise(&run).unwrap(); // This should fail
/// ```
pub struct ExerciseDescriptorDb(PickleDb);

impl ExerciseDescriptorDb {
    /// Creates a new instance of ExerciseDescriptorDb database which has de JSON format.
    ///  
    /// If the file `name` already exists it is overwritten.
    ///
    /// # Argument
    ///
    /// * `name` - the name of the databse which is the filename of the db.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::exercise_descriptor::ExerciseDescriptorDb;
    ///
    /// ExerciseDescriptorDb::new("my_awesome_db.db");
    /// ```
    pub fn new(name: &str) -> Self {
        ExerciseDescriptorDb(PickleDb::new_json(name, PickleDbDumpPolicy::AutoDump))
    }

    /// Loads an instance of [`ExerciseDescriptorDb`] which is located in the file `name`.
    ///
    /// # Argument
    ///
    /// * `name` - the name of the database to be loaded.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::exercise_descriptor::ExerciseDescriptorDb;
    ///
    /// ExerciseDescriptorDb::new("my_awesome_db.db");
    ///
    /// ExerciseDescriptorDb::load("my_awesome_db.db").unwrap();
    /// ```
    ///
    /// ```should_panic
    /// use engine::exercise_descriptor::ExerciseDescriptorDb;
    ///
    /// ExerciseDescriptorDb::load("not_awesome_db.db").unwrap();
    /// ```

    pub fn load(name: &str) -> Result<Self, ExerciseDescriptorDbError> {
        Ok(ExerciseDescriptorDb(PickleDb::load_json(
            name,
            PickleDbDumpPolicy::AutoDump,
        )?))
    }

    /// Tries to load an instance of [`ExerciseDescriptorDb`] which is located in the file `name`.
    /// If no file exists a new file is created.
    ///
    /// # Argument
    ///
    /// * `name` - the name of the database to be loaded/created.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::exercise_descriptor::ExerciseDescriptorDb;
    ///
    /// ExerciseDescriptorDb::load_or_new("a_new_awesome_db.db");
    /// ```
    pub fn load_or_new(name: &str) -> Self {
        let db = PickleDb::load_json(name, PickleDbDumpPolicy::AutoDump);
        match db {
            Ok(out) => ExerciseDescriptorDb(out),
            Err(_) => ExerciseDescriptorDb::new(name),
        }
    }

    /// Add an exercise into the Db.
    /// If the exercise id already exists in the Db the exercise is replaced.
    /// There is a small possibility [`PickleDb`] returns an error related to the insertion into
    /// the Db.
    ///
    /// # Examples
    ///
    /// ```
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// # use engine::exercise_descriptor::ExerciseDescriptorDb;
    /// let mut db = ExerciseDescriptorDb::new("ex.db");
    /// let rest = ExerciseDescriptor::new("Rest")
    ///     .set_description("Do nothing");
    ///
    /// db.add_or_replace_exercise(&rest).unwrap(); // Ok
    /// ```
    ///
    /// ```
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// # use engine::exercise_descriptor::ExerciseDescriptorDb;
    /// # let mut db = ExerciseDescriptorDb::new("ex_add_or_replace.db");
    /// let rest = ExerciseDescriptor::new("Rest")
    ///    .set_description("Do nothing");
    /// let rest_more = ExerciseDescriptor::new("Rest")
    ///     .set_description("Do nothing, but better");
    /// db.add_or_replace_exercise(&rest).unwrap();      // Created "Rest" entry
    /// db.add_or_replace_exercise(&rest_more).unwrap(); // Replaced "Rest"
    /// ```
    pub fn add_or_replace_exercise(
        &mut self,
        ex: &ExerciseDescriptor,
    ) -> Result<(), ExerciseDescriptorDbError> {
        self.0.set(&ex.get_id(), ex)?;
        Ok(())
    }

    /// Add an exercise into the Db.
    /// If the exercise id already exists an error is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// # use engine::exercise_descriptor::ExerciseDescriptorDb;
    /// let mut db = ExerciseDescriptorDb::new("ex_try_add_ok.db");
    /// let rest = ExerciseDescriptor::new("Rest")
    ///     .set_description("Do nothing");
    ///
    /// db.try_add_exercise(&rest).unwrap(); // Ok
    /// ```
    ///
    /// ```should_panic
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// # use engine::exercise_descriptor::ExerciseDescriptorDb;
    /// let mut db = ExerciseDescriptorDb::new("ex_try_add_err.db");
    /// let rest = ExerciseDescriptor::new("Rest")
    ///     .set_description("Do nothing");
    ///
    /// let rest_more = ExerciseDescriptor::new("Rest")
    ///     .set_description("Do nothing, but better");
    ///
    /// db.try_add_exercise(&rest).unwrap(); // Ok
    /// db.try_add_exercise(&rest_more).unwrap(); // Error
    /// ```
    pub fn try_add_exercise(
        &mut self,
        ex: &ExerciseDescriptor,
    ) -> Result<(), ExerciseDescriptorDbError> {
        if !self.0.exists(&ex.get_id()) {
            self.0.set(&ex.get_id(), ex)?;
            Ok(())
        } else {
            Err(ExerciseDescriptorDbError::KeyAlreadyPresentError(
                ex.get_id(),
            ))
        }
    }

    /// Returns an exercise form the Db with a given key.
    ///
    /// # Argument
    ///
    /// - key: the key of the ExerciseDescriptor to get the actual descriptor, returns None if key
    /// not present.
    ///
    /// # Examples
    ///
    /// ```
    /// # use engine::exercise_descriptor::ExerciseDescriptor;
    /// # use engine::exercise_descriptor::ExerciseDescriptorDb;
    /// let mut db = ExerciseDescriptorDb::new("ex_try_add_ok.db");
    /// let rest = ExerciseDescriptor::new("Rest")
    ///     .set_description("Do nothing");
    ///
    /// db.try_add_exercise(&rest).unwrap(); // Ok
    /// let ex = db.get_exercise("Rest");
    /// assert_eq!(ex, Some(rest));
    /// ```
    pub fn get_exercise(&self, key: &str) -> Option<ExerciseDescriptor> {
        self.0.get::<ExerciseDescriptor>(key)
    }
}

/// The Error handling for the ExerciseDescriptorDb
#[derive(Debug, Error)]
pub enum ExerciseDescriptorDbError {
    /// A key is already present in the Db
    #[error("The key {0} is already present in the Db.")]
    KeyAlreadyPresentError(String),

    /// Propagating the error form PickleDb opening
    #[error(transparent)]
    PickleDbError(#[from] pickledb::error::Error),
}
