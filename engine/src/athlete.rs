#![deny(missing_docs)]
//! This module contains various informations about the athletes and their training.
//!
//! It is split into the [`Athlete`] structure which contains all the relevant information about
//! one athlete, like identitfication (name, age, ...) and athletic capabilities
//! (VO2 max for example) and performances for example.
//!
//! It will also also contain the database of athletes along with the traning they have performed
//! on which dates. This information will help determine the load and progression of the athletes.

use crate::length::Meters;
use crate::weight::Kilograms;
use ::serde::{Deserialize, Serialize};
use chrono::NaiveDate;
use email_address_parser::EmailAddress;
use pickledb::{PickleDb, PickleDbDumpPolicy};
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
#[cfg(feature = "cli")]
use structopt::StructOpt;
use thiserror::Error;

/// Coordinates of a person which only contains naming: [`Coordinates::name`],
/// [`Coordinates::surname`], date of birth [`Coordinates::date_birth`], and e-mail
/// address [`Coordinates::email`].
///
/// The [`Athlete`]s are uniquely identified with the e-mail address if available or by a
/// combination of name, surname, and date of birth if no e-mail is available.
#[derive(Debug, Serialize, Deserialize, Hash)]
struct Coordinates {
    /// First name of a person
    name: String,
    /// Surname of a person
    surname: String,
    /// Optional date of birth of a person
    date_birth: NaiveDate,
    /// Optional e-mail address of a person
    email: Option<String>,
}

impl Coordinates {
    fn from_name_surname_date(name: &str, surname: &str, date_birth: NaiveDate) -> Self {
        Coordinates {
            name: String::from(name),
            surname: String::from(surname),
            date_birth,
            email: None,
        }
    }

    /// We need to hash the coordinates to create a unique identifier.
    #[allow(dead_code)]
    fn compute_hash(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }

    /// Sets the e-mail but before we check the validity of the e-mail (is it of the form
    /// foo@bar.baz).
    /// At some point we could also check if the e-mail is valid by using the crate
    /// [`check-if-email-exists`].
    fn and_email(mut self, email: &str) -> Result<Self, AthleteError> {
        if !Coordinates::is_email_valid(email) {
            Err(AthleteError::EmailError(String::from(email)))
        } else {
            self.email = Some(email.into());
            Ok(self)
        }
    }

    fn is_email_valid(email: &str) -> bool {
        EmailAddress::parse(email, None).is_some()
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    use chrono::NaiveDate;

    #[test]
    fn test_and_email() {
        Coordinates::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2016, 7, 8))
            .and_email("jean-paul.dax@blamail.com")
            .unwrap();
    }

    #[test]
    #[should_panic]
    fn test_and_email_failing() {
        Coordinates::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2016, 7, 8))
            .and_email("jean-paul@dax@blamail.com")
            .unwrap();
    }

    #[test]
    fn test_and_email_no_dot() {
        Coordinates::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2016, 7, 8))
            .and_email("jean-paul@daxblamailcom")
            .unwrap();
    }

    #[test]
    fn test_compute_hash() {
        let c1 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 8),
        );
        let c2 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 8),
        );
        assert_eq!(c1.compute_hash(), c2.compute_hash());
    }

    #[test]
    fn test_compute_hash_email() {
        let c1 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 8),
        )
        .and_email("jean-paul@dax.com")
        .unwrap();
        let c2 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 8),
        )
        .and_email("jean-paul@dax.com")
        .unwrap();
        assert_eq!(c1.compute_hash(), c2.compute_hash());
    }

    #[test]
    #[should_panic]
    fn test_compute_hash_failing_date() {
        let c1 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 8),
        );
        let c2 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 9),
        );
        assert_eq!(c1.compute_hash(), c2.compute_hash());
    }

    #[test]
    #[should_panic]
    fn test_compute_hash_failing_name() {
        let c1 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 9),
        );
        let c2 = Coordinates::from_name_surname_date(
            "Jean",
            "-PaulDax",
            NaiveDate::from_ymd(2016, 7, 9),
        );
        assert_eq!(c1.compute_hash(), c2.compute_hash());
    }

    #[test]
    #[should_panic]
    fn test_compute_hash_failing_email() {
        let c1 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 9),
        )
        .and_email("jean-paul@dax.com")
        .unwrap();
        let c2 = Coordinates::from_name_surname_date(
            "Jean-Paul",
            "Dax",
            NaiveDate::from_ymd(2016, 7, 9),
        )
        .and_email("jean-paul@dax.ch")
        .unwrap();
        assert_eq!(c1.compute_hash(), c2.compute_hash());
    }
}

/// Physical characeterostics only describe sizes that are not related to training.
#[derive(Debug, Serialize, Deserialize)]
struct PhysicalCharacteristics {
    /// The weight is given in Kilograms
    weight: Option<Kilograms>,
    /// The height is given in Centimeters
    height: Option<Meters>,
}

impl PhysicalCharacteristics {
    /// By default all the fields are `None`
    fn new() -> Self {
        PhysicalCharacteristics {
            weight: None,
            height: None,
        }
    }

    /// Adds a new [`PhysicalCharacteristics::weight`] to a [`PhysicalCharacteristics`]
    /// The `weight` is in `kilograms`.
    fn add_weight(mut self, weight: f64) -> Result<Self, PhysicalCharacteristicsError> {
        if weight > 20.0 && weight < 150.0 {
            self.weight = Some(Kilograms::from_kilograms(weight)?);
            Ok(self)
        } else {
            Err(PhysicalCharacteristicsError::InvalidWeightError(weight))
        }
    }

    /// Adds a new [`PhysicalCharacteristics::height`] to a [`PhysicalCharacteristics`]
    /// The `height` is in `kilograms`.
    fn add_height(mut self, height: f64) -> Result<Self, PhysicalCharacteristicsError> {
        if height > 1.4 && height < 2.20 {
            self.height = Some(Meters::from_meters(height)?);
            Ok(self)
        } else {
            Err(PhysicalCharacteristicsError::InvalidHeightError(height))
        }
    }

    /// Computes the Body Mass Index which is given by
    /// BMI = weight [kg] / (height [m])^2 [kg/m^2].
    #[allow(dead_code)]
    fn compute_bmi(&self) -> Result<f64, PhysicalCharacteristicsError> {
        match (&self.weight, &self.height) {
            (None, None) => Err(PhysicalCharacteristicsError::MissingHeightAndWeightError),
            (None, Some(_h)) => Err(PhysicalCharacteristicsError::MissingWeightError),
            (Some(_w), None) => Err(PhysicalCharacteristicsError::MissingHeightError),
            (Some(w), Some(h)) => Ok(f64::from(w) / f64::from(h) * f64::from(h)),
        }
    }
}

/// The Error manager for PhysicalCharacteristics
#[derive(Error, Debug)]
pub enum PhysicalCharacteristicsError {
    /// The positivity of the length
    #[error(transparent)]
    PositivityLengthError(#[from] crate::length::LengthError),

    /// The positivity of the weight
    #[error(transparent)]
    PositivityWeightError(#[from] crate::weight::WeightError),

    /// The invalid height error (people can't have absurd heights)
    #[error("The height must always be in between 1.4 and 2.2 meter, but got {0}")]
    InvalidHeightError(f64),

    /// The invalid weight error (people can't have absurd weights)
    #[error("The weight must always be in between 20.0 and 150.0 kilograms, but got {0}")]
    InvalidWeightError(f64),

    /// The height is missing
    #[error("The height is missing in the physical characteristics.")]
    MissingHeightError,

    /// The weight is missing
    #[error("The weight is missing in the physical characteristics.")]
    MissingWeightError,

    /// The height is missing
    #[error("The height and weight are missing in the physical characteristics.")]
    MissingHeightAndWeightError,
}

/// Physiological characteristics of an [`Athlete`].
/// All members may be unknown so they are Options.
#[derive(Debug, Serialize, Deserialize)]
struct PhysiologicalCharacteristics {
    /// Optional VO2 max
    vo2_max: Option<i64>,
    /// Optional minimal cardiac frequency in beats per minute
    min_cardiac_freq: Option<i64>,
    /// Optional maximal cardiac frequency in beats per minute
    max_cardiac_freq: Option<i64>,
}

impl PhysiologicalCharacteristics {
    /// By default all values are None
    fn new() -> Self {
        PhysiologicalCharacteristics {
            vo2_max: None,
            min_cardiac_freq: None,
            max_cardiac_freq: None,
        }
    }
    /// Sets vo2 max and validates its values before returning.
    fn and_vo2_max(mut self, vo2: i64) -> Result<Self, PhysiologicalCharacteristicsError> {
        if !(10..=200).contains(&vo2) {
            Err(PhysiologicalCharacteristicsError::Vo2maxError(vo2))
        } else {
            self.vo2_max = Some(vo2);
            Ok(self)
        }
    }

    /// Sets vo2 max and validates its values before returning.
    fn and_min_cardiac_freq(
        mut self,
        min_cf: i64,
    ) -> Result<Self, PhysiologicalCharacteristicsError> {
        if !(10..=250).contains(&min_cf) {
            Err(PhysiologicalCharacteristicsError::MinCardiacFreqError(
                min_cf,
            ))
        } else {
            self.min_cardiac_freq = Some(min_cf);
            Ok(self)
        }
    }

    /// Sets vo2 max and validates its values before returning.
    fn and_max_cardiac_freq(
        mut self,
        max_cf: i64,
    ) -> Result<Self, PhysiologicalCharacteristicsError> {
        if !(10..=250).contains(&max_cf) {
            Err(PhysiologicalCharacteristicsError::MaxCardiacFreqError(
                max_cf,
            ))
        } else {
            self.max_cardiac_freq = Some(max_cf);
            Ok(self)
        }
    }
}

/// The Error manager for PhysiologicalCharacteristics
#[derive(Error, Debug)]
pub enum PhysiologicalCharacteristicsError {
    /// Vo2 max value must be within physiological range
    #[error("The VO2 max must always be in between 10 and 200, but got {0}")]
    Vo2maxError(i64),

    /// Minimum cardiac frequency value must be within physiological range
    #[error("The minimum cardiac frequency must always be in between 10 and 250, but got {0}")]
    MinCardiacFreqError(i64),

    /// Maximum cardiac frequency value must be within physiological range
    #[error("The maximum cardiac frequency must always be in between 10 and 250, but got {0}")]
    MaxCardiacFreqError(i64),
}

/// The [`AthleteBuilder`] struct contains various information that are required
/// to build an [`Athlete`] struct from a CLI.
///
/// There are three mandatory fields to construct an [`Athlete`]:
///
/// * a name
/// * a surname
/// * a date of birth
///
/// # Examples
///
/// ```no_run
/// # use engine::athlete::AthleteBuilder;
/// # use structopt::StructOpt;
///
/// let athlete = AthleteBuilder::from_args()
///        .finalize().unwrap();
/// ```
#[cfg(feature = "cli")]
#[derive(Debug, StructOpt)]
#[structopt(name = "basic")]
pub struct AthleteBuilder {
    /// The first name
    #[structopt(short, long)]
    name: String,
    /// The surname
    #[structopt(short, long)]
    surname: String,
    /// The birthdate in the format %d-%m-%Y (ex. 17-10-1971)
    #[structopt(short, long = "birthdate")]
    birth_date: String,
    /// The email, of the for name@domain
    #[structopt(short, long)]
    email: Option<String>,
    /// The height is given in Kilograms (a float), optional
    #[structopt(short, long)]
    weight: Option<f64>,
    /// The height is given in Meters (a float), optional
    #[structopt(short, long)]
    height: Option<f64>,
    /// The vo2 max value (an integer), optional
    #[structopt(short, long)]
    vo2: Option<i64>,
    /// The minimal cardiac frequency in beats per minute (an integer), optional
    #[structopt(long)]
    min_freq: Option<i64>,
    /// The maximal cardiac frequency in beats per minute (an integer), optional
    #[structopt(long)]
    max_freq: Option<i64>,
}

#[cfg(feature = "cli")]
impl AthleteBuilder {
    /// Return an Athlete from the AthleteBuilder::from_args() structopt function if everything
    /// went fine. Returns an error if there was an error.
    ///
    /// ```no_run
    /// # use engine::athlete::AthleteBuilder;
    /// # use structopt::StructOpt;
    ///
    /// let athlete = AthleteBuilder::from_args().finalize().unwrap();
    /// ```
    pub fn finalize(self) -> Result<Athlete, AthleteError> {
        let date_birth: NaiveDate = NaiveDate::parse_from_str(&self.birth_date, "%d-%m-%Y")?;
        let athlete = Athlete::from_name_surname_date(&self.name, &self.surname, date_birth);
        let athlete = match self.email {
            Some(email) => athlete.and_email(&email)?,
            None => athlete,
        };
        let athlete = match self.weight {
            Some(w) => athlete.and_weight(w)?,
            None => athlete,
        };
        let athlete = match self.height {
            Some(h) => athlete.and_height(h)?,
            None => athlete,
        };
        let athlete = match self.vo2 {
            Some(vo2) => athlete.and_vo2_max(vo2)?,
            None => athlete,
        };
        let athlete = match self.min_freq {
            Some(freq) => athlete.and_min_cardiac_freq(freq)?,
            None => athlete,
        };
        let athlete = match self.max_freq {
            Some(freq) => athlete.and_max_cardiac_freq(freq)?,
            None => athlete,
        };
        Ok(athlete)
    }
}

/// The [`Athlete`] struct contains various information about a person that will perform the
/// training. It will also help monitor the evolution of said athlete.
///
/// The physical and physiological characteristics are optional since they may not be known at all
/// times.
///
/// After each addition into the [`Athlete`] struct its `Athlete` field in the database is updated.
///
/// # Examples
///
/// ```no_run
/// # use engine::athlete::{Athlete, AthleteError};
/// # use chrono::NaiveDate;
/// # fn main() -> Result<(), AthleteError> {
/// let athlete = Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
///     .and_weight(80.0)?.and_height(1.72)?.and_email("jean-paul@dax.com")?
///     .and_vo2_max(47)?.and_min_cardiac_freq(53)?.and_max_cardiac_freq(190)?;
/// Ok(())
/// # }
/// ```
#[derive(Debug, Serialize, Deserialize)]
pub struct Athlete {
    /// Contains name, address, and e-mail
    coords: Coordinates,
    /// The physical characteristics of the [`Athlete`], height, wight, BMI for example.
    phys_char: Option<PhysicalCharacteristics>,
    /// The physiological characteristics of the [ Athlete ], VO2 max, minimal/maximal cardiac
    /// frequency for example.
    physio_char: Option<PhysiologicalCharacteristics>,
}

impl Athlete {
    /// Import Athlete from Database. The database may not exist resulting in an Error.
    /// Also the database may not contain an athlete resulting again in an Error.
    ///
    /// Fills a complete [`Athlete`] field.
    ///
    /// # Argument
    ///
    /// - name: the name of the database to load the athlete from
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::athlete::Athlete;
    ///
    /// let athlete = Athlete::from_db("my_athlete.db").unwrap();
    /// ```
    pub fn from_db(name: &str) -> Result<Self, AthleteError> {
        let content = PickleDb::load_json(name, PickleDbDumpPolicy::AutoDump)?;

        content
            .get::<Athlete>("Athlete")
            .ok_or_else(|| AthleteError::MissingKeyError(String::from("Athlete")))
    }

    /// The id of an athlete that is used for now for the creation of its databse is of the form
    /// `<name>_<surname>_<birth_date>`.
    fn get_athlete_id(&self) -> String {
        format!(
            "{}_{}_{}",
            self.coords.name, self.coords.surname, self.coords.date_birth
        )
    }

    /// The name of the db of the athlete which is formed from `get_athlete_id().db`.
    fn get_db_name(&self) -> String {
        format!("{}.db", self.get_athlete_id())
    }

    /// Create a new [`Athlete`] from name/surname and date of birth which are **mandatory**
    /// fields. The other fields [`Athlete::phys_char`] and [`Athlete::physio_char`] are `None` by default.
    ///
    /// # Arguments
    ///
    /// * `name` - first name of the athlete;
    /// * `surname` - surname of the athlete;
    /// * `birth_date` - birth date of the athlete.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete = Athlete::from_name_surname_date("Jean-Paul", "Dax",
    ///     NaiveDate::from_ymd(2001, 10, 8));
    /// ```
    pub fn from_name_surname_date(name: &str, surname: &str, date_birth: NaiveDate) -> Self {
        Athlete {
            coords: Coordinates::from_name_surname_date(name, surname, date_birth),
            phys_char: None,
            physio_char: None,
        }
    }

    /// Sets an e-mail. The e-mail format is validated and an `Error` is returned if the format is
    /// invalid.
    ///
    /// # Arguments
    ///
    /// * `email` - a valid e-mail, otherwise an error will be returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
    ///         .and_email("jp@dax.com").unwrap();
    /// ```
    pub fn and_email(mut self, email: &str) -> Result<Self, AthleteError> {
        self.coords = self.coords.and_email(email)?;
        Ok(self)
    }

    /// Sets the weight. The weight is validated and an `Error` is returned if the value is
    /// invalid.
    ///
    /// # Arguments
    ///
    /// * `weight` - a valid weight value, otherwise an error will be returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
    ///         .and_weight(80.8).unwrap();
    /// ```
    pub fn and_weight(mut self, weight: f64) -> Result<Self, PhysicalCharacteristicsError> {
        self.phys_char = Some(
            self.phys_char
                .unwrap_or_else(PhysicalCharacteristics::new)
                .add_weight(weight)?,
        );
        Ok(self)
    }

    /// Sets the height. The height is validated and an `Error` is returned if the value is
    /// invalid.
    ///
    /// # Arguments
    ///
    /// * `height` - a valid height value, otherwise an error will be returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
    ///         .and_height(1.78).unwrap();
    /// ```
    pub fn and_height(mut self, height: f64) -> Result<Self, PhysicalCharacteristicsError> {
        self.phys_char = Some(
            self.phys_char
                .unwrap_or_else(PhysicalCharacteristics::new)
                .add_height(height)?,
        );
        Ok(self)
    }

    /// Sets the vo2 max. The vo2 max is validated and an `Error` is returned if the value is
    /// invalid.
    ///
    /// # Arguments
    ///
    /// * `vo2` - a valid vo2 value, otherwise an error will be returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
    ///         .and_vo2_max(47).unwrap();
    /// ```
    pub fn and_vo2_max(mut self, vo2: i64) -> Result<Self, PhysiologicalCharacteristicsError> {
        self.physio_char = Some(
            self.physio_char
                .unwrap_or_else(PhysiologicalCharacteristics::new)
                .and_vo2_max(vo2)?,
        );
        Ok(self)
    }

    /// Sets the min cardiac freuency. The minimum frequency is validated and an `Error` is returned if the value is
    /// invalid.
    ///
    /// # Arguments
    ///
    /// * `min_cardiac_freq` - a valid minimum cardiac frequency value, otherwise an error will be returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
    ///         .and_min_cardiac_freq(47).unwrap();
    /// ```
    pub fn and_min_cardiac_freq(
        mut self,
        min_cardiac_freq: i64,
    ) -> Result<Self, PhysiologicalCharacteristicsError> {
        self.physio_char = Some(
            self.physio_char
                .unwrap_or_else(PhysiologicalCharacteristics::new)
                .and_min_cardiac_freq(min_cardiac_freq)?,
        );
        Ok(self)
    }

    /// Sets the max cardiac freuency. The maxmium frequency is validated and an `Error` is returned if the value is
    /// invalid.
    ///
    /// # Arguments
    ///
    /// * `max_cardiac_freq` - a valid maximum cardiac frequency value, otherwise an error will be returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8))
    ///         .and_max_cardiac_freq(191).unwrap();
    /// ```
    pub fn and_max_cardiac_freq(
        mut self,
        max_cardiac_freq: i64,
    ) -> Result<Self, PhysiologicalCharacteristicsError> {
        self.physio_char = Some(
            self.physio_char
                .unwrap_or_else(PhysiologicalCharacteristics::new)
                .and_max_cardiac_freq(max_cardiac_freq)?,
        );
        Ok(self)
    }

    /// The [`Athlete`] database contains the [`Athlete::coords`], the [`Athlete::physio_char`],
    /// and [`Athlete::physio_char`]. The [`Athlete::coords`] must be updated in place, since their
    /// evolution in time is not important and only the most recent values are relevant. The other
    /// two might evolve in time so they must be saved with different entries into the database.
    ///
    /// The database is overwritten if pre-existing or created a new if not.
    ///
    /// The database is named according to the function get_db_name() which at the moment is given
    /// by `<name>_<surname>_<birth_date>.db`.
    ///
    /// # Examples
    ///
    /// Jean-Paul Dax, born on the 8th of October 2001 would be stored in the database named:
    /// `Jean-Paul_Dax_2001-10-08.db`
    ///
    /// ```
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8));
    /// athlete.dump_to_db();
    /// ```
    pub fn dump_to_db(&self) -> Result<(), AthleteError> {
        let db = PickleDb::load_json(&self.get_db_name(), PickleDbDumpPolicy::AutoDump);
        match db {
            Ok(mut content) => {
                content.set::<Athlete>("Athlete", self)?;
            }
            Err(_) => {
                PickleDb::new_json(&self.get_db_name(), PickleDbDumpPolicy::AutoDump)
                    .set::<Athlete>("Athlete", self)?;
            }
        };
        Ok(())
    }

    /// The [`Athlete`] database contains the [`Athlete::coords`], the [`Athlete::physio_char`],
    /// and [`Athlete::physio_char`]. The [`Athlete::coords`] must be updated in place, since their
    /// evolution in time is not important and only the most recent values are relevant. The other
    /// two might evolve in time so they must be saved with different entries into the database.
    ///
    /// In this version if the database already exists an error is produced.
    ///
    /// # Examples
    ///
    /// Jean-Paul Dax, born on the 8th of October 2001 would be stored in the database named:
    /// `Jean-Paul_Dax_2001-10-08.db`
    ///
    /// ```no_run
    /// use engine::athlete::Athlete;
    /// use chrono::NaiveDate;
    ///
    /// let athlete =
    ///     Athlete::from_name_surname_date("Jean-Paul", "Dax", NaiveDate::from_ymd(2001, 10, 8));
    /// athlete.try_dump_to_db().unwrap(); // this would fail if Jean-Paul_Dax_2001-10-08.db
    ///                                    // already exists
    /// ```
    pub fn try_dump_to_db(&self) -> Result<(), AthleteError> {
        let db = PickleDb::load_json(&self.get_db_name(), PickleDbDumpPolicy::AutoDump);
        match db {
            Ok(_) => Err(AthleteError::AlreadyExistingDbError(self.get_db_name())),
            Err(_) => {
                PickleDb::new_json(&self.get_db_name(), PickleDbDumpPolicy::AutoDump)
                    .set::<Athlete>("Athlete", self)?;
                Ok(())
            }
        }
    }
}

/// The Error manager for Athlete
#[derive(Error, Debug)]
pub enum AthleteError {
    /// Valid physical characteristics
    #[error(transparent)]
    PhysicalCharacteristicsError(#[from] PhysicalCharacteristicsError),

    /// Valid physical characteristics
    #[error(transparent)]
    PhysiologicalCharacteristicsError(#[from] PhysiologicalCharacteristicsError),

    /// The date must be in a correct format
    #[error(transparent)]
    DateError(#[from] chrono::format::ParseError),

    /// Email error
    #[error("The email must be of the form login@domain but got {0}")]
    EmailError(String),

    /// Error with the database already existing
    #[error("The database {0} aleary exists. We are not overwriting it.")]
    AlreadyExistingDbError(String),

    /// Propagating the error form PickleDb opening
    #[error(transparent)]
    PickleDbError(#[from] pickledb::error::Error),

    /// Certain keys must be present in the Athlete's Db
    #[error("{0} key not found in database.")]
    MissingKeyError(String),
}
