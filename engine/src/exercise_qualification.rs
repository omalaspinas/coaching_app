//! In this module there are all the diffrent qualifications for an exercise:
//!
//! * The [`Quantity`] which can be of three different variants:
//!     * A given [`Quantity::Time`] which represents a certain duration (from seconds to hours).
//!     * A given [`Quantity::Distance`] which represents a distance in [`Meters`]
//!     * A given amount of [`Quantity::Repetitions`] which is just a number.
//! * The [`Intensity`] which can be also of three different variants:
//!     * A given [`Intensity::Percentage`] which is the integer percentage of maximum possible effort.
//!     * A given [`Intensity::Target`] which is a target time to achieve.
//!     * A given [`Intensity::ExtraCharge`] which is an additional charge to add in kilograms.
use crate::duration::Duration;
use crate::length::Meters;
use crate::weight::Kilograms;
use thiserror::Error;

/// The `Quantity` that is bound to an exercise is either a given `Time` (a `Duration`), a given `Distance` (in `Meters`),
/// or a certain amount of `Repetitions` (represented by a positive integer).
#[derive(Debug, Clone)]
pub enum Quantity {
    /// The duration of an exercise.
    Time(Duration),
    /// The distance over which an exercise must be performed.
    Distance(Meters),
    /// The number of repetition an exercise must be performed.
    Repetitions(i64),
    /// Sometimes there is no meaning in having a quantity
    None,
}

/// Functions implemented to interact with the different quantities of each [`crate::Exercise`].
impl Quantity {
    /// Constructs a `None` variant of [`Quantity`] when there is no meaning
    /// in adding a quantity.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Quantity;
    ///
    /// let q = Quantity::none();
    /// ```
    pub fn none() -> Self {
        Quantity::None
    }
    /// Constructs a [`Quantity`] from hours, minutes and seconds.
    /// If hours, minutes, and seconds are invalid an error is returned
    /// (hours >= 0, 0 <= minutes <= 59, 0 <= seconds <= 59 are Ok).
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Quantity;
    ///
    /// let q = Quantity::from_hours_minutes_seconds(10, 10, 10).unwrap();
    /// ```
    pub fn from_hours_minutes_seconds(h: i64, m: i64, s: i64) -> Result<Self, QuantityError> {
        Ok(Quantity::Time(Duration::from_hours_minutes_seconds(
            h, m, s,
        )?))
    }

    /// Constructs a [`Quantity`] from meters.
    ///
    /// If the meters are invalid (only >= 0 are Ok) an error is returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Quantity;
    ///
    /// let q = Quantity::from_meters(1500.0).unwrap();
    /// ```
    pub fn from_meters(m: f64) -> Result<Self, QuantityError> {
        Ok(Quantity::Distance(Meters::from_meters(m)?))
    }

    /// Constructs a [`Quantity`] from repetitions which are [`i64`].
    ///
    /// If the repetitions are invalid (only > 0 are Ok) an error is returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Quantity;
    ///
    /// let q = Quantity::from_repetitions(15).unwrap();
    /// ```
    pub fn from_repetitions(r: i64) -> Result<Self, QuantityError> {
        if r > 0 {
            Ok(Quantity::Repetitions(r))
        } else {
            Err(QuantityError::RepetitionsError(r))
        }
    }
}

/// The formatter for the [`Quantity`] enum.
impl std::fmt::Display for Quantity {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Quantity::Time(d) => write!(f, "{}", d),
            Quantity::Distance(m) => write!(f, "{}", m),
            Quantity::Repetitions(r) => {
                if *r > 0 {
                    write!(f, "{} times", r)
                } else {
                    write!(f, "")
                }
            }
            Quantity::None => write!(f, ""),
        }
    }
}

/// The Error manager for Quantity
#[derive(Error, Debug)]
pub enum QuantityError {
    /// The positivity of the length
    #[error(transparent)]
    LengthError(#[from] crate::length::LengthError),

    /// Validity of duration
    #[error(transparent)]
    DurationError(#[from] crate::duration::DurationError),

    /// Repetitions must be positive
    #[error("The number of repetitions must be positive, now they are {0}")]
    RepetitionsError(i64),
}

/// The [`Intensity`] of a given [`crate::exercise_descriptor::ExerciseDescriptor`]. It is given in terms of a `Percentage` of
/// maximum intensity, of a `Target` duration, or an additional `ExtraCharge` to be added.
/// Sine sometimes there is no meaning in adding an inensity for an exercise (e.g. Rest) we also
/// have a `None` intensity.
#[derive(Debug, Clone)]
pub enum Intensity {
    /// The percentage of maximum capacity.
    Percentage(i64),
    /// The target time of much reach.
    Target(Duration),
    /// The extra charge to be added for the exercise.
    ExtraCharge(Kilograms),
    /// Sometimes an Intensity makes no sense, so there is None.
    None,
}

/// Functions implemented to interact with the different quantities of each [`crate::Exercise`].
impl Intensity {
    /// Constructs an [`Intensity`] from a [`enum@Intensity::Percentage`] of maximal capacity.
    /// Returns an error if the percentage is not in the 0..100 range.
    ///
    /// # Argument
    ///
    /// - p: the value of the percentage which is an integer in [0..100] otherwise an error is
    /// returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Intensity;
    ///
    /// let i = Intensity::from_percentage(10).unwrap();
    /// ```
    pub fn from_percentage(p: i64) -> Result<Self, IntensityError> {
        if (0..=100).contains(&p) {
            Ok(Intensity::Percentage(p))
        } else {
            Err(IntensityError::InvalidPercentageError(p))
        }
    }

    /// Constructs a [`Intensity`] from a [`Duration`] for a [`enum@Intensity::Target`].
    /// Returns an error if the durantion is not valid.
    ///
    /// # Argument
    ///
    /// - p: the value of the percentage which is an integer in [0..100] otherwise an error is
    /// returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Intensity;
    ///
    /// let i = Intensity::from_hours_minutes_seconds(1, 10, 7).unwrap();
    /// ```
    pub fn from_hours_minutes_seconds(h: i64, m: i64, s: i64) -> Result<Self, IntensityError> {
        Ok(Intensity::Target(Duration::from_hours_minutes_seconds(
            h, m, s,
        )?))
    }

    /// Constructs a [`Quantity`] from [`Kilograms`].
    ///
    /// # Argument
    ///
    /// - k: the number of kilograms in a floating point value (valid is >= 0).
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Intensity;
    /// # use engine::weight::Kilograms;
    ///
    /// let i = Intensity::from_kilograms(110.2).unwrap();
    /// ```
    pub fn from_kilograms(k: f64) -> Result<Self, IntensityError> {
        Ok(Intensity::ExtraCharge(Kilograms::from_kilograms(k)?))
    }

    /// Constructs an [`Intensity`] from a [`enum@Intensity::None`].
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::exercise_qualification::Intensity;
    ///
    /// let i = Intensity::none();
    /// ```
    pub fn none() -> Self {
        Intensity::None
    }
}

/// The formatter for the [`Intensity`] enum.
impl std::fmt::Display for Intensity {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Intensity::Target(t) => write!(f, "{}", t),
            Intensity::ExtraCharge(m) => write!(f, "{}", m),
            Intensity::Percentage(r) => write!(f, "{} percent", r),
            Intensity::None => write!(f, ""), // we write nothing for None
        }
    }
}

/// The Error manager for Intensity
#[derive(Error, Debug)]
pub enum IntensityError {
    /// If the percentage is not between 0 and 100
    #[error("A percentage is always between 0 and 100, but got {0}")]
    InvalidPercentageError(i64),

    /// A problem with the Duration
    #[error("transparent")]
    DurationError(#[from] crate::duration::DurationError),

    /// A problem with the Weight
    #[error("transparent")]
    WeightError(#[from] crate::weight::WeightError),
}
