#![deny(missing_docs)]
//! This module contains weight metrics in in kilograms.
//!
//! This will be useful for all the exercises that are based on weightliftings.
use serde::{Deserialize, Serialize};
use thiserror::Error;

/// A struct to help with weigth in exercises which is measured in Kilograms.
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Kilograms(f64);

/// `Kilograms` implements only a very limited amount of functions.
///
/// Kilograms can be constructed from grams or kilograms.
///
/// # Examples:
///
/// ```
/// use engine::weight::Kilograms;
///
/// let two_point_five = Kilograms::from_kilograms(2.5).unwrap();
/// assert_eq!(two_point_five, Kilograms::from_grams(2_500).unwrap());
/// ```
impl Kilograms {
    /// Constructs Kilograms from a floating point number
    /// in kilograms.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::weight::Kilograms;
    ///
    /// let weight = Kilograms::from_kilograms(80.0).unwrap(); // 80kg weight
    /// ```
    pub fn from_kilograms(kg: f64) -> Result<Self, WeightError> {
        if kg >= 0.0 {
            Ok(Kilograms(kg))
        } else {
            Err(WeightError::PositivityError(kg))
        }
    }

    /// Contructs Kilograms from grams which is an `u32`.
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use engine::weight::Kilograms;
    ///
    /// let weight = Kilograms::from_grams(80).unwrap(); // 0.080kg weight
    /// ```
    pub fn from_grams(grams: i64) -> Result<Self, WeightError> {
        Kilograms::from_kilograms(grams as f64 / 1000.0)
    }
}

/// The formatter for the [`Kilograms`] struct.
impl std::fmt::Display for Kilograms {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} kilograms", self.0)
    }
}

/// Implementation of the From trait
impl From<&Kilograms> for f64 {
    fn from(item: &Kilograms) -> Self {
        item.0
    }
}

/// The Error manager for weight
#[derive(Error, Debug)]
pub enum WeightError {
    /// The positivity of the weight
    #[error("The weight must always be positive, but got {0}")]
    PositivityError(f64),
}
