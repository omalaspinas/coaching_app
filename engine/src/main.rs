use engine::exercise_descriptor::ExerciseDescriptor;
use engine::exercise_qualification::{Intensity, Quantity};
use engine::{Exercise, Repetition, TrainingSession};
// use serde_json::Result;
use anyhow::{Context, Result};

fn main() -> Result<()> {
    let ex_desc = ExerciseDescriptor::new("Run")
        .set_description(&String::from("The name is self explanatory. Just run."));
    let quantity = Quantity::from_meters(1000.0).context(format!(
        "Unable to create a quantity from {} meters",
        1000.0
    ))?;
    let intensity = Intensity::from_percentage(75)
        .context(format!("Unable to create an intesity from {}", 75))?;
    let run_ex = Exercise::new(ex_desc, quantity, intensity);
    println!("Exercise: {:?}", run_ex);

    let ex_desc = ExerciseDescriptor::new("Rest")
        .set_description(&String::from("The name is self explanatory. Just run."));
    let quantity = Quantity::from_meters(100.0)
        .context(format!("Unable to create a quantity from {}", 100.0))?;
    let intensity = Intensity::none();
    let rest_ex = Exercise::new(ex_desc, quantity, intensity);
    println!("Exercise: {:?}", rest_ex);

    let rep = Repetition::new()
        .add_exercise(run_ex)
        .add_exercise(rest_ex)
        .set_amount(7)?;
    let training = TrainingSession::new().add_repetition(rep);
    println!("traning session {:?}", training);

    Ok(())
}
