// SPDX-FileCopyrightText: 2020 Robin Krahl <robin.krahl@ireas.org>
// SPDX-License-Identifier: CC0-1.0

//! This example generates a demo PDF document and writes it to the path that was passed as the
//! first command-line argument.  You may have to adapt the `FONT_DIRS`, `DEFAULT_FONT_NAME` and
//! `MONO_FONT_NAME` constants for your system so that these files exist:
//! - `{FONT_DIR}/{name}-Regular.ttf`
//! - `{FONT_DIR}/{name}-Bold.ttf`
//! - `{FONT_DIR}/{name}-Italic.ttf`
//! - `{FONT_DIR}/{name}-BoldItalic.ttf`
//! for `name` in {`DEFAULT_FONT_NAME`, `MONO_FONT_NAME`}.

use crate::{Format, TrainingSession};
use chrono::Local;
use genpdf::fonts::{Font, FontFamily};
use genpdf::style::Style;
use genpdf::Alignment;
use genpdf::Document;
use genpdf::Element;
use genpdf::{elements, fonts, style};
use thiserror::Error;

const FONT_DIRS: &[&str] = &[
    "/usr/share/fonts/liberation",
    "/usr/share/fonts/truetype/liberation",
];
const DEFAULT_FONT_NAME: &str = "LiberationSans";

struct BoxedElement(Box<dyn Element>);

impl Element for BoxedElement {
    fn render(
        &mut self,
        context: &genpdf::Context,
        area: genpdf::render::Area<'_>,
        style: style::Style,
    ) -> Result<genpdf::RenderResult, genpdf::error::Error> {
        self.0.render(context, area, style)
    }
}

/// This structure contains the helper functions to build a PDF document out of a traning session.
pub struct PdfGenerator {
    doc: Document,
    default: FontFamily<Font>,
    default_bold: Style,
    default_italic: Style,
}

impl PdfGenerator {
    /// Fonts finding may fail so this function returns a result
    pub fn try_new() -> Result<Self, PdfGeneratorError> {
        // Fonts management
        let font_dir = FONT_DIRS
            .iter()
            .find(|path| std::path::Path::new(path).exists())
            .ok_or(PdfGeneratorError::DirectoryError)?;
        let default_font =
            fonts::from_files(font_dir, DEFAULT_FONT_NAME, Some(fonts::Builtin::Helvetica))?;

        let mut doc = genpdf::Document::new(default_font.clone());
        let default = doc.add_font_family(default_font);
        let default_bold = style::Style::from(default).bold();
        let default_italic = style::Style::from(default).italic();

        // I don't know what that means bit it's in the example
        doc.set_minimal_conformance();
        // Default line spacing 1.25
        doc.set_line_spacing(1.25);

        Ok(Self {
            doc,
            default,
            default_bold,
            default_italic,
        })
    }

    fn add_break(mut self) -> Self {
        self.doc.push(elements::Break::new(1.5));
        self
    }

    fn add_title(mut self, title: &str) -> Self {
        self.doc.set_title(title);
        // Defines a title for the document (centered, with text `title`, bold and 20 points fonts size)
        self.doc.push(
            elements::Paragraph::new(title)
                .aligned(Alignment::Center)
                .styled(style::Style::new().bold().with_font_size(20)),
        );

        self
    }

    fn add_subtitle(mut self, title: &str) -> Self {
        self.doc.set_title(title);
        // Defines a title for the document (centered, with text `title`, bold and 20 points fonts size)
        self.doc.push(
            elements::Paragraph::new(title)
                .aligned(Alignment::Center)
                .styled(style::Style::new().bold().with_font_size(15)),
        );

        self
    }

    fn add_decorator(mut self) -> Self {
        let mut decorator = genpdf::SimplePageDecorator::new();
        decorator.set_margins(10);
        // This sets the header on each page except on the first one because it would overlap with the
        // actual tite.
        decorator.set_header(|page| {
            let mut layout = elements::LinearLayout::vertical();
            if page > 1 {
                layout.push(
                    elements::Paragraph::new(format!("Page {}", page)).aligned(Alignment::Center),
                );
                layout.push(elements::Break::new(1));
            }
            layout.styled(style::Style::new().with_font_size(10))
        });
        self.doc.set_page_decorator(decorator);
        self
    }

    fn add_default_paragraph(mut self, text: &str) -> Self {
        self.doc
            .push(elements::Paragraph::default().styled_string(text, self.default));
        self
    }

    fn from_formatted_string(&self, f_str: Format) -> Result<BoxedElement, PdfGeneratorError> {
        Ok(match f_str {
            Format::Default(a) => BoxedElement(Box::new(
                elements::Paragraph::default().styled_string(a, self.default),
            )),
            Format::Bold(a) => BoxedElement(Box::new(
                elements::Paragraph::default().styled_string(a, self.default_bold),
            )),
            Format::Italic(a) => BoxedElement(Box::new(
                elements::Paragraph::default().styled_string(a, self.default_italic),
            )),
            Format::Image(a) => match crate::http::img_from_str(&a) {
                None => BoxedElement(Box::new(
                    elements::Paragraph::default().styled_string("No image.", self.default_italic),
                )),
                Some((p, _format)) => {
                    BoxedElement(Box::new(elements::Image::from_dynamic_image(p)?))
                }
            },
        })
    }

    fn add_training_session(mut self, ts: &TrainingSession) -> Result<Self, PdfGeneratorError> {
        let mut list = elements::LinearLayout::vertical();
        for (e, item) in ts.iter().enumerate() {
            let mut rep = item.lines();
            let mut elem = elements::LinearLayout::vertical().element(
                elements::Paragraph::default()
                    .styled_string(format!("Exercise {} ", e + 1), self.default_bold),
            );
            // This contains "Repeat N times"
            let el_rep = self.from_formatted_string(rep.pop().unwrap())?;
            if item.num_repetitions() > 1 {
                // Push the amount of repetitions
                elem.push(el_rep);
            }

            // If there are more than 1 exercise in the rep
            if rep.len() > 1 {
                // A list is needed here because there are more than one exercise in the
                // repetition
                let mut local_list = elements::OrderedList::new();
                for e in rep {
                    let el = self.from_formatted_string(e)?;
                    local_list = local_list.element(el);
                }
                list.push(elem.element(local_list));
            } else {
                // We don't want a list since there is only one exercise in the rep
                // Also el_rep is discarded.
                elem.push(self.from_formatted_string(rep.pop().unwrap())?);
                list.push(elem);
            }

            list.push(elements::Break::new(1.5))
        }
        self.doc.push(list);

        Ok(self)
    }

    fn add_page_break(mut self) -> Self {
        // A page break
        self.doc.push(genpdf::elements::PageBreak::new());
        self
    }

    fn add_description(mut self, ts: &TrainingSession) -> Result<Self, PdfGeneratorError> {
        // The proportional respective width each column
        // This also defines the number of columns used in the table
        let weights = vec![1, 2, 2, 2];
        let num_cols: u32 = weights.len() as u32;
        let mut img_table = elements::TableLayout::new(weights);
        img_table.set_cell_decorator(elements::FrameCellDecorator::new(true, false, true));
        for desc in ts.get_exercise_descriptors().iter() {
            let mut row = img_table.row();
            for c in desc.columns(num_cols) {
                row.push_element(self.from_formatted_string(c)?);
            }

            row.push()?;
        }
        self.doc.push(img_table);

        Ok(self)
    }

    /// Generate a pdf with a given name
    pub fn from_ts(fname: &str, ts: &TrainingSession) -> Result<(), PdfGeneratorError> {
        let pdf = PdfGenerator::try_new()?
            .add_decorator()
            .add_title(&format!(
                "Training session of {}",
                Local::today().naive_local()
            ))
            .add_break()
            .add_default_paragraph(
                "In this traning session we are going to perform the foloowing Repetitions.",
            )
            .add_break()
            .add_training_session(ts)?
            .add_page_break()
            .add_subtitle("For more details on the exercises")
            .add_break()
            .add_description(ts)?;

        pdf.doc.render_to_file(fname)?;
        Ok(())
    }
}

/// The Error manager for Pdf Generation
#[derive(Error, Debug)]
pub enum PdfGeneratorError {
    /// The positivity of the length
    #[error(transparent)]
    GenpdfError(#[from] genpdf::error::Error),

    /// The FONT_DIR can't be found in filesystem
    #[error("Could not find any font directories provided.")]
    DirectoryError,
}
