#![deny(missing_docs)]
//! This module is used for the duration of an exercise.
//!
//! Only seconds, minutes and hours are the relevant time scales for any exercises.

use thiserror::Error;

/// Reimport of the [`chrono::Duration`] crate for local use and to limit the amount of
/// functionalities that are not needed.
#[derive(Debug, Clone, PartialEq)]
pub struct Duration(chrono::Duration);

impl Duration {
    /// Create a new duration from hours, minutes, and seconds. If invalid values
    /// for hours, minutes or seconds are provided then a Error is returned.
    ///
    /// # Arguments
    ///
    /// - hours: how many hours are there in the duration (>= 0)
    /// - minutes: how many minutes are there in the duration (>= 0 && <= 59)
    /// - seconds: how many seconds are there in the duration (>= 0 && <= 59)
    ///
    /// # Example
    ///
    /// In the following example we create a duration of 10 hours, 27 minutes and 9 seconds
    /// ```no_run
    /// # use engine::duration::Duration;
    ///
    /// let d = Duration::from_hours_minutes_seconds(10, 27, 9).unwrap();
    /// ```
    pub fn from_hours_minutes_seconds(
        hours: i64,
        minutes: i64,
        seconds: i64,
    ) -> Result<Self, DurationError> {
        Duration::try_hours(hours)?;
        Duration::try_minutes(minutes)?;
        Duration::try_seconds(seconds)?;

        Ok(Duration(
            chrono::Duration::hours(hours)
                + chrono::Duration::minutes(minutes)
                + chrono::Duration::seconds(seconds),
        ))
    }

    /// Create a new duration from minutes, and seconds. If invalid values
    /// for minutes or seconds are provided then a Error is returned.
    ///
    /// # Arguments
    ///
    /// - minutes: how many minutes are there in the duration (>= 0 && <= 59)
    /// - seconds: how many seconds are there in the duration (>= 0 && <= 59)
    ///
    /// # Example
    ///
    /// In the following example we create a duration of 27 minutes and 9 seconds
    /// ```no_run
    /// # use engine::duration::Duration;
    ///
    /// let d = Duration::from_minutes_seconds(27, 9).unwrap();
    /// ```
    pub fn from_minutes_seconds(minutes: i64, seconds: i64) -> Result<Self, DurationError> {
        Duration::try_minutes(minutes)?;
        Duration::try_seconds(seconds)?;

        Ok(Duration(
            chrono::Duration::minutes(minutes) + chrono::Duration::seconds(seconds),
        ))
    }

    /// Create a new duration from seconds. If invalid values
    /// for seconds are provided then a Error is returned.
    ///
    /// # Arguments
    ///
    /// - seconds: how many seconds are there in the duration (>= 0 && <= 59)
    ///
    /// # Example
    ///
    /// In the following example we create a duration of 9 seconds
    /// ```no_run
    /// # use engine::duration::Duration;
    ///
    /// let d = Duration::from_seconds(9).unwrap();
    /// ```
    /// Create a new duration from seconds
    pub fn from_seconds(seconds: i64) -> Result<Self, DurationError> {
        Duration::try_seconds(seconds)?;

        Ok(Duration(chrono::Duration::seconds(seconds)))
    }

    /// Check if hours are in the correct range (larger than 0)
    fn try_hours(hours: i64) -> Result<(), DurationError> {
        if hours < 0 {
            Err(DurationError::HoursError(hours))
        } else {
            Ok(())
        }
    }

    /// Check if hours are in the correct range (larger than 0)
    fn try_minutes(minutes: i64) -> Result<(), DurationError> {
        if (0..=59).contains(&minutes) {
            Ok(())
        } else {
            Err(DurationError::MinutesError(minutes))
        }
    }

    /// Check if hours are in the correct range (larger than 0)
    fn try_seconds(seconds: i64) -> Result<(), DurationError> {
        if (0..=59).contains(&seconds) {
            Ok(())
        } else {
            Err(DurationError::SecondsError(seconds))
        }
    }
}

/// The formatter for the [`Duration`] enum.
impl std::fmt::Display for Duration {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let hours = self.0.num_hours();
        let minutes = (self.0 - chrono::Duration::hours(hours)).num_minutes();
        let seconds =
            (self.0 - chrono::Duration::hours(hours) - chrono::Duration::minutes(minutes))
                .num_seconds();
        match (hours, minutes, seconds) {
            (h @ 1..=23, 0, 0) => write!(f, "{} hours", h),
            (h @ 1..=23, m @ 1..=59, 0) => write!(f, "{} hours, {} minutes", h, m),
            (h @ 1..=23, m @ 1..=59, s @ 1..=59) => {
                write!(f, "{} hours, {} minutes, {} seconds", h, m, s)
            }
            (0, m @ 1..=59, 0) => write!(f, "{} minutes", m),
            (0, m @ 1..=59, s @ 1..=59) => write!(f, "{} minutes, {} seconds", m, s),
            (0, 0, s @ 1..=59) => write!(f, "{} seconds", s),
            (0, 0, 0) => write!(f, ""),
            (_, _, _) => panic!("Error with the duration format."),
        }
    }
}

/// The Error manager for Quantity
#[derive(Error, Debug)]
pub enum DurationError {
    /// Hours must be positive
    #[error("Hours must be positive, now they are {0}")]
    HoursError(i64),

    /// Minutes must be between 0 and 59
    #[error("Minutes must be in the 0 ... 59 range, now they are {0}")]
    MinutesError(i64),

    /// Seconds must be between 0 and 59
    #[error("Seconds must be in the 0 ... 59 range, now they are {0}")]
    SecondsError(i64),
}
