use anyhow::{Context, Result};
use engine::athlete::AthleteBuilder;
use structopt::StructOpt;

fn main() -> Result<()> {
    let athlete = AthleteBuilder::from_args()
        .finalize()
        .context("Error while creating an Athlete.")?;
    athlete
        .try_dump_to_db()
        .context("Error while dumping the database.")?;
    println!("Athete: {:?}", athlete);
    Ok(())
}
