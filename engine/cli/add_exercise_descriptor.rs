use anyhow::{Context, Result};
use engine::exercise_descriptor::ExerciseDescriptorBuilder;
use structopt::StructOpt;

fn main() -> Result<()> {
    // let ex = ExerciseDescriptorBuilder::from_args().finalize();
    ExerciseDescriptorBuilder::from_args()
        .finalize()
        .context("Error creating an exercise descriptor.")?;
    Ok(())
}
