# Application de sport

Le but de ce projet est de créer une application de  planning d'entraînements  
complètement open source, multi-plateformes, qui soit simple d'utilisation pour 
le sportif et le coach.

Le coach doit pouvoir facilement créer des séances et le sportif doit pouvoir 
facilement lire ces séances. Idéalement, les séances sont communiquées au 
sportif de façon chiffrées de bout en bout et les applications sportif ou coach 
doivent pouvoir fonctionner hors-ligne.

# Structure

Pour une application d'entraînement nous avons besoin d'un certain nombre 
d'informations qui sont données comme entrées au programme globalement: infos 
sur le **sportif**, liste d'**exercices** par exemple. A partir de ces 
informations le coach pourra créer des **séances** d'entraînement pour un 
**sportif** qui contiendront des **exercices**. Le programme pourra d'une part 
produire un fichier d'entraînement (récapitulatif d'une séance, avec les 
exercices, leur durée, le nombre de répétitions, etc), calculer la **charge** 
sur le sportif, et éventuellement un calendrier avec les séances. Est-ce qu'on 
voudrait aussi avoir la possibilité de faire des tests pour suivre l'évolution 
des perfs?

# Sportif

## Informations d'entrée sur le sportif

* Nom/prénom
* Taille
* Poids
* Age
* VO2
* Pulsations/poids max.

# Entraînement

Une **séance** d'entraînement est constituée d'un certain nombre de **séries** 
d'exercices. Chaque série est une **suite ordonnée** **d'exercices** répétés un 
certain nombre de fois, avec une **durée**, une **distance** ou un **nombre de 
répétitions** (appelons ça une quantité) et une **intensité** qui peut aussi 
être complété par un **commentaire**.

## Exercice

* Nom,
* Description,
* Photos (liste en base64?),
* Vidéo (lien ou en dur).

Il est important que chaque exercice ait un descriptif et un visuel permettant 
au sportif d'effectuer au mieux l'exercice que le coach lui donne. Pour ce 
faire on peut imaginer une série d'images avec un descriptif ou carrément une 
vidéo. Il faut ensuite réfléchir où et comment ces supports visuels sont 
stockés.

Pour les vidéos, il serait souhaitable qu'elles soient téléchargeables en ligne 
(lien youtube p.ex.) que le sportif pourrait visualiser depuis un téléphone ou 
un ordinateur. Pour les images, il faudrait qu'elles soient directement dans 
l'entraînement que le sportif a à effectuer le coût de stockage n'étant pas 
trop élevé normalement.

Un exercice peut également être vu comme une combinaison de plusieurs exercices 
/ avec une quantité.

## Quantité

* Durée
* ou distance
* ou nombre de répétitions

Chaque exercice a une notion de quantité d'exercice à faire: cela peut être une 
durée donnée, une distance (s'il s'agit de course à pied ou de vélo par 
exemple), et un nombre de répétitions (s'il s'agit de musculation par exemple).

La quantité peut aussi être un nombre de répétitions de plusieurs exercices. 
Par exemple, une série de sprints et de repos. 

## Intensité

* Pourcentage de pulse max.
* Charge additionnelle.

Pour chaque exercice on définit une intensité. Cette intensité peut être soit 
liée aux pulsations maximum du sportif ou au poids maximum qu'il peut avoir 
pour un exercice. Ces deux valeurs maximales devraient être stockées et mises à 
jour pour chaque sportif dans l'application du coach.

# Pour le sportif

## Export

* Vue de la séance (smartphone?)
* Intégration avec le calendrier
* Export en PDF pour impression.
* Intégration avec une smartwatch?

Le sportif doit pouvoir avoir une vue simple de chacun de ses entraînements. La 
version la plus simple serait un export sous format PDF (ou HTML) de la séance 
d'entrainement. Il doit également pouvoir planifier simplement les séances dans 
son calendrier. Dans un second temps, on peut imaginer une meilleure 
intégration avec un smartphone ou un smartwatch.

## Format pour le sportif

* Vue linéaire avec la durée/nombre de répétition des exercices avec les images 
  (quand dispo).
* Durée totale des exercices.
* Vue de l'intensité/régime de la séance.

Dans un premier temps, la vue doit simplement contenir les exercices dans 
l'ordre dans lesquels ils doivent être effectués, ainsi que toutes les 
informations de durée/intensité. Ensuite, on peut imaginer intégrer un 
chronomètre, un compteur de pulsations, etc à intégrer avec une application 
smartphone par exemple.

# Pour le coach

* Entrées des infos des sportifs.
* Entrées des exercices avec persistance.
* Entrées des séances avec persistance et calculs des différents régimes.
* Possibilité d'avoir des statistiques sur les entraînements de chaque sportif.
* Suivi de l'évolution des performances du sportif?

Le coach doit pouvoir maintenir à jour les données de chacun de ses sportif. Il 
doit pouvoir insérer facilement des exercices, les rechercher, les modifier, en 
créer de nouveau à partir d'autres plus anciens déjà enregistrés, etc. Il doit 
pouvoir consulter des statistiques pour chaque sportif (quel type d'exercices, 
quelle intensité, etc). Il doit aussi pouvoir suivre l'évolution en cours 
d'année du sportif.

# Impératifs techniques

* Multi-plateformes (Windows, macos, linux) pour le coach.
* Mlti-plateformes (Windows, macos, linux, Android, IOS) pour le sportif.
* Backup simple
* Import/export exercices simples.
* Chiffrement?
* Fonctionnement "offline".

Il est important que cette l'application coach fonctionne aussi bien sous 
windows que sur mac (et linux dans une moindre mesure). Il n'y a pas besoin de 
la faire fonctionner sur mobile dans un premier temps, éventuellement sur 
tablette. En revanche il est important que l'application du sportif puisse 
fonctionner sur portable. Les applications doivent pouvoir fonctionner 
raisonnablement "hors-ligne", les sauvgardes globales doivent être simples, les 
imports/exports de séances ou d'exercices doivent être aisés.

On peut commencer à réfléchir pour chiffrer les données stockées localement par 
défaut, et également les données transmises aux sportifs.

# Possible libraries

* [Serde](https://serde.rs/)
* [Egui](https://github.com/emilk/egui)
* [Genpdf](https://sr.ht/~ireas/genpdf-rs/)
